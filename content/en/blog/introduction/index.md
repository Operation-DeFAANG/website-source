---
title: "Introduction"
description: "Introducing Operation DeFAANG"
excerpt: "Introducing Operation DeFAANG, FAANG | LESS, and the I-100"
date: 2022-11-06T10:42:00-04:00
lastmod: 2022-11-06T10:42:00-04:00
draft: false 
weight: 50
images: ["Operation-DeFAANG.png"]
categories: []
tags: []
contributors: ["Andy"]
pinned: false
homepage: false
---

# Welcome

Welcome to Operation DeFAANG! 

The project currently exists as a student-driven school project, but has prospects to be much more. 

## A Problem Exists
Big Tech companies don’t allow users to escape the collection and storage of personal data. Self hosting is a good alternative, however, manually setting up and configuring a server is exceedingly tedious and time consuming – especially for first timers. Lack of understanding of the command line prevents and wards away individuals from self-hosting.

## A Solution Is Offered
Operation DeFAANG is trying to take a jab at that market share and control by helping users set up their own home servers to run things similar to what these tech giants offer. We are aiming to provide a quick and easy way for individuals to host their own Google Drives on their own computers.

## Project Structure
The project is split into four phases with individual assignments within each phase. 

Through completing these assignments, along with self-assigned ones, Operation DeFAANG aims to build FAANG | LESS, an all-in-one self-hosting tool, and the I-100, a modular, small form factor PC case.  
