---
title: "Introduction"
description: "Introducing Phase 04"
lead: "Where it all ends"
date: 2022-11-06T16:50:14-05:00
lastmod: 2022-11-06T16:50:14-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "introduction-454bce88ae3c94bf2d30eba218401394"
weight: 510
toc: true
---

## Phase 04
After over seven months of work, the end is here.

There is no development-focused work being done here, just a presentation and reflection of what we’ve done.

Part of the project is an exposition of all of the work the different student groups completed. 
We present all of our hardwork, our blood, sweat, and tears, and let it be judged.

Then we take a step back. We look at everything that we’ve done and just reflect.
