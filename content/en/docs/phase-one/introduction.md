---
title: "Introduction"
description: "Introducing Phase 01"
lead: "Where it all begins"
date: 2022-11-06T16:50:01-05:00
lastmod: 2022-11-06T16:50:01-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "introduction-f82d9b42b0c510cb384139ea444e8ea3"
weight: 210
toc: true
---

## Phase 01
We’ve organised into groups, but that doesn’t mean we’re organised. We still need to figure a few things out.

The idea behind Phase 01 is that we determine who we are as a group, as a project, and what we are actually doing and how it fares in the world.
