---
title: "Problem and Solution"
description: "Part One and Two of Phase 01"
lead: "There's a problem. We're offering a solution"
date: 2022-11-17T13:20:16-05:00
lastmod: 2022-11-17T13:20:16-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "problem-and-pitch-6a9bb58729523057b84df53e3bbef27b"
weight: 211 
toc: true
---

## The Problem 
Big Tech companies don’t allow users to escape the collection and storage of personal data. 
Self hosting is a good alternative, however, manually setting up and configuring a server is exceedingly tedious and time consuming -- especially for first timers. 

## Our Solution
Operation DeFAANG is trying to take a jab at Big Tech's market share and control by helping users set up their own home servers to run things similar to what these tech giants offer through FAANG | LESS and the I-100. 
We are aiming to provide a quick and easy way for individuals to host their own Google Drives on their own computers.
