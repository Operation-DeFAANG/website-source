---
title: "Polling and Research"
description: "Parts Five and Seven of Phase 01"
lead: "Learning from the people and the markets"
date: 2022-11-22T13:01:21-05:00
lastmod: 2022-11-22T13:01:21-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "polling-and-research-b08500c8bc80529471402b38f27a5dac"
weight: 213
toc: true
---

## Market Research
### Big Tech
Facebook reportedly has [1.968 million daily active users](https://www.statista.com/statistics/346167/facebook-global-dau/). 

Apple reportedly has over [125 million daily active Apple News users](https://techcrunch.com/2020/04/30/apple-news-hits-125-million-monthly-active-users/) and [over 60 million Apple Music users](https://www.theverge.com/2019/6/27/18761603/apple-music-60-million-subscribers-eddy-cue-spotify). Additionally, they recently [reportedly over $123.9 billion in revenue](https://www.apple.com/newsroom/pdfs/FY22_Q1_Consolidated_Financial_Statements.pdf). 

Microsoft recently reported [over 250 million active Teams users](https://www.windowscentral.com/microsoft-teams-has-scored-250-million-monthly-active-users) and a [quarterly revenue of $51.9 billion for Q4](https://www.microsoft.com/en-us/Investor/earnings/FY-2022-Q4/press-release-webcast) of the 2022 fiscal year. Additionally, their Windows operating system accounts for [over 95% of Steam users](https://store.steampowered.com/hwsurvey/), a very large amount considering the service recently reported [over 31.9 million concurrent users](https://store.steampowered.com/charts/). 

Google reportedly has [over 1 billion active Google Drive users](https://www.theverge.com/2018/7/25/17613442/google-drive-one-billion-users), [over 2 billion active YouTube users](https://backlinko.com/youtube-users), [over 50 million YouTube Premium and Music subscribers](https://www.theverge.com/2021/9/2/22654318/youtube-50-million-premium-music-subscribers-streaming-services), and [over 2 billion monthly Android users](https://www.theverge.com/2017/5/17/15654454/android-reaches-2-billion-monthly-active-users). 

### Alternatives
There are some tools that exist that help users set up and manage servers, but they all appear to have some sort of flaw. 

Some have odd and/or annoying requirements. While most of these requirements have ways around them -- though it would be tedious and potential difficult for a novice to get around -- others are full stop requirements. 
These requirements may be things needed to set up the server or things that you have to host in order to run the software. 

## Polling
Our polling showed a large percentage of people not knowing what self-hosting is with a general mixed feeling towards Big Tech, though primarily positive leaning in the middle. 

An issue we noticed was that, to 75% of people we polled, self-hosting was perceived as being difficult with some interest existing in self-hosting.
