---
title: "Design Brief"
description: "Parts Three, Eight, and Nine of Phase 01"
lead: "What's our plan?"
date: 2022-11-17T13:27:33-05:00
lastmod: 2022-11-17T13:27:33-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "design-brief-c52d131b7f2ab1ad6c24bb5c33ed58e6"
weight: 212
toc: true
---

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

## Problem
Big Tech companies don’t allow users to escape the collection and storage of personal data. 
Self hosting is a good alternative, however, manually setting up and configuring a server is exceedingly tedious and time consuming – especially for first timers.

## Objective
Operation DeFAANG aims to help users set up their own home servers to run things similar to what tech giants, like Google, offer. 
We are working to provide individuals with the ability to run their own servers with ease in small, modular computer cases.

## Target Audience
* People with stable internet
* People who are okay with less social media
* People who like technology
* People who like learning

## Planned Milestones
### Phase 01
Complete setting up the project and start with research and development 

Target Date: 4 October 2022

### Phase 02
Complete a functioning demo for FAANG | LESS and start on a 3D model of the I-100

Target Date: 1 December 2022

### Phase 03
Complete FAANG | LESS and a physical build of the I-100

Target Date: 21 February 2023

## Criteria and Constraints
We need FAANG | LESS to be distro-agnostic, meaning it can run on any Linux distribution, and to have a graphical user interface as opposed to a terminal or command line interface. The latter is simply for easier on-boarding of new users.
We want FAANG | LESS to take up less than 500MB for faster installations, though are willing to sacrifice this for greater performance and a better user experience. 

The I-100 is aimed to be able to cool a quad-core x86 CPU while remaining near room temperature without becoming louder than 25 decibles. 
We also need the I-100 to maintain a base size of 20 litres so that it would qualify as a small form factor case. 

## Budget
Development of FAANG | LESS should be completely free, same with the final release. 

We hope to not spend more than $500 on the I-100 itself, though the components required to test the case in a real-world test could cost up to $2,000.
