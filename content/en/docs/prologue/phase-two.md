---
title: "Phase 02"
description: "Research / Develop"
lead: "This is where the fun begins"
date: 2022-11-06T15:21:40-05:00
lastmod: 2022-11-06T15:21:40-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "phase-two-ac2b8ebefcd847021fb02eed245276c0"
weight: 120
toc: true
---

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

## Research / Develop
The purpose of this phase is pretty self-explanatory: start work on developing a prototype. 

We've figured out who we are, how we're going to run things, and what we're going to do; now we need to do it. 

We need to research how to accomplish our goals and any laws, codes, and/or regulations that may prevent us from doing so. We then need to actually start building things. 

By the end, we personally hope to finish at least a functioning prototype of FAANG | LESS and a 3D model of the I-100. 

## Deliverables
_[to be added]_
