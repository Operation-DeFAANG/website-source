---
title: "Introduction"
description: "Introducing the Phase Structure"
lead: "Everything is divided into phases"
date: 2022-11-06T14:56:37-05:00
lastmod: 2022-11-06T14:56:37-05:00
draft: false 
images: []
menu:
  docs:
    parent: ""
    identifier: "introduction-b2484e4f9e8a93c1bc58303e44a10db2"
weight: 100 
toc: true
---

## Phases
As a school project, there are certain requirements that we must fulfill and an order to the otherwise madness.

### Phase 00
Otherwise known as the 'Pre-Design' phase. 

During this phase, we are yet to become a group. We are still determining what we want to do and who we want to work with. 

As this is largely independent and separate from the project, there will be no section dedicated to Phase 00.

### Phase 01
This is where the documentation really starts. 

At this point, we've organised into a group with a project idea. 
We've created a contract, determined how we will record information and documentation, and have started working on brainstorming. 

The 'Define / Brainstorm' phase, we are still yet to start with prototyping and are more so just getting together our bearings and making sure we actually have something. 

### Phase 02

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

Where the fun begins. 

The premise of Phase 02 is very clear based on its name: the Research / Develop phase. 
We've now collected data, completed some preliminary research, and are now starting to make prototypes. 

By the end of this phase, we hope to accomplish at least a basic and functioning outline of FAANG | LESS and a 3D model of the I-100. 

### Phase 03
Where the stress starts to hit. 

Testing and building is the aim of the 'Test / Build' phase, clearly. 
A prototype has been built and now we just need to build and test it. 

In addition to recording everything that happens during building and testing, we also need to record the cost of everything.

### Phase 04
When the sense of pride and accomplishment washes over us. 

'Evaluate / Reflect'. It is as simple as that. 
We've spent the last seven or so months working on this project; it's time to just take a step back and take everything in. 

<br />

> The mission, the nightmares. They're finally... over.

ARC-5555, 'Fives'
