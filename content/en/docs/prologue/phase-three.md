---
title: "Phase 03"
description: "Build / Test"
lead: "Where the stress starts to hit"
date: 2022-11-06T15:21:51-05:00
lastmod: 2022-11-06T15:21:51-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "phase-three-5ff697935697977ccd9190db7daa6a7a"
weight: 130
toc: true
---

{{< alert icon="⚠️" text="It was at this point in which development for the I-100 was halted. Expect no mentions of its development from here on" />}}

## Build / Test
The goals of this phase appear similar to Phase 02. The difference is that a close-to-final product must be made. 

Phase 02's priority was researching different aspects of developing a product and making initial prototypes. 
Phase 03 is aimed at using that research and the experience from prototyping to develop a near-final product that could be released to the public. 

If final products cannot be made, an explanation as to why and a plan for making it could also be delivered. 

## Deliverables
_[to be added]_
