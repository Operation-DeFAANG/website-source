---
title: "Phase 01"
description: "Define / Brainstorm"
lead: "The first step into madness"
date: 2022-11-06T15:15:08-05:00
lastmod: 2022-11-06T15:15:08-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "phase-one-cab2607c6adb9ba012e420e11158b5d4"
weight: 110
toc: true
---

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

## Define / Brainstorm
We've organised into groups, but that doesn't mean we're _organised_. 
We still need to figure a few things out. 

This idea behind Phase 01 is that we determine who we are as a group, as a project, and what we are actually doing and how it fares in the world.

## Deliverables
### Design Brief
Our target market is people attempting to leave the Big Tech ecosystems, but need a bit of help in getting things set up on their own. 
We plan on helping them through FAANG | LESS as a software tool and the I-100 as an unintrusive case for the system.

We plan on having a functioning prototype of FAANG | LESS completed by December 2022 and a 3D model of the I-100 by the same time. 
We hope to have a v1.0 release of FAANG | LESS and a physical prototype of the I-100 by February 2023. 

### Market Research
While performing market research, we polled individuals as well as determining what alternatives exist and what they offer.

#### Polling
During our polling, we confirmed our suspicions that the average consumer does not look at self-hosting as a viable option. 

From the start, we knew that we would most likely be targetting people who were just getting into systems administration and needed something to act as an aide. 

#### Alternatives
The currently existing alternatives mainly are in the form of all-in-one software tools, though there are some all-in-one hardware and software systems. 
The issue with those that we reviewed is that they have odd software limitations and requirements and/or are too expensive.

On the software side, we plan on having no, or minimal and non-intruding, limitations and requirements. 
For the case, we don't currently plan on selling it, but rather having plans available for users to build their own with their own materials.

