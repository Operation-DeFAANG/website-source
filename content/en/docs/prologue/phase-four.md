---
title: "Phase 04"
description: "Evaluate / Reflect"
lead: "The culmination of seven months of work"
date: 2022-11-06T15:21:55-05:00
lastmod: 2022-11-06T15:21:55-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "phase-four-0fc0d27a6c0d580fb94c403b0baed2b3"
weight: 140
toc: true
---

## Evaluate / Reflection
After over seven months of work, the end is here. 

There is no development-focused work being done here, just a presentation and reflection of what we've done. 

Part of the project is an exposition of all of the work the different student groups completed. 
We present all of our hardwork, our blood, sweat, and tears, and let it be judged. 

Then we take a step back. We look at everything that we've done and just reflect. 

## Deliverables
_[to be added]_
