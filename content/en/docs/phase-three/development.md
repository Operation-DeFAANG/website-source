---
title: "Development"
description: "Part Nine of Phase 03"
lead: "We're doing work, we promise"
date: 2023-03-24T10:02:55-04:00
lastmod: 2023-03-24T10:02:55-04:00
draft: false 
images: []
menu:
  docs:
    parent: ""
    identifier: "development-b973135aa4cd4e944ff7be37c5d9dec5"
weight: 413 
toc: true
---

## Initial Setup Script
Currently found [here](https://codeberg.org/Operation-DeFAANG/kits), the initial setup script will check for apt and then install Docker, Tailscale, and NGINX. 
The user will be guided through Tailscale's setup by its own tool while the server setups NGINX's reverse proxy by itself.

## Kits
Kits can be found [here](https://codeberg.org/Operation-DeFAANG/kits).

### Media Server
The media server kit is planned to support [Jellyfin](https://jellyfin.org/).

#### Jellyfin
_TBD_

### Cloud Storage Server
Currently supported in the cloud storage kit are [File Browser](https://github.com/filebrowser/filebrowser), [Nextcloud](https://nextcloud.com/), and [Owncloud](https://owncloud.com/).

#### File Browser
_TBD_

#### Nextcloud
Currently, the script includes Nextcloud Basic and All-In-One via Docker Compose files. 

#### Owncloud
_TBD_
