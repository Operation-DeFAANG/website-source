---
title: "Testing"
description: "Parts Seven, Eight, Ten of Phase 03"
lead: "Testing FAANG | LESS"
date: 2023-03-24T09:41:14-04:00
lastmod: 2023-03-24T09:41:14-04:00
draft: false 
images: []
menu:
  docs:
    parent: ""
    identifier: "testing-4bfd2ed982559feb1575955e425c1aba"
weight: 412 
toc: true
---

## Procedures

### General Usage
General usage testing will include the time it takes to do a first install and the power consumed by a running machine to compare against the cost of subscriptions. 

We hope for the time taken to be less than ten minutes and the power consumption to turn out to be less than $20/m based on the United States [median cost of electricity](https://www.eia.gov/electricity/state/) in 2021 of $0.11. 

#### Steps
1. Begin monitoring of power consumption, Being stopwatch
2. Begin FAANG | LESS installation
   * Mark stopwatch when installation finishes
3. Install Jellyfin and Nextcloud
   * Mark stopwatch when installation finishes
   * Chosen for likelihood of usage
4. Run server for three hours
5. Graph power consumption

### Installations
Installations testing will test the functionality of FAANG | LESS. Included in the testing will be the installation and setup by the initial setup script and then two kits, Jellyfin and Nextcloud. 

Tested will be Ubuntu 22.04 LTS, Ubuntu Server 22.04 LTS, Debian 11.6, and Fedora Workstation 37. 
We hope for the first three operating systems to install and run successfully with the last failing and quiting due to the current focus on systems using the apt package manager.

#### Steps
1. Run FAANG | LESS initial setup
2. Install Jellyfin
3. Install Nextcloud

## Results
_TBD_
