---
title: "Introduction"
description: "Introducing Phase 03"
lead: "Where the stress begins"
date: 2022-11-06T16:50:10-05:00
lastmod: 2022-11-06T16:50:10-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "introduction-9ee3bbace52d0c43421149b8a044f883"
weight: 410
toc: true
---

## Phase 03
The goals of this phase appear similar to Phase 02. The difference is that a close-to-final product must be made.

Phase 02’s priority was researching different aspects of developing a product and making initial prototypes. 
Phase 03 is aimed at using that research and the experience from prototyping to develop a near-final product that could be released to the public.

If final products cannot be made, an explanation as to why and a plan for making it could also be delivered.
