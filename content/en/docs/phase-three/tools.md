---
title: "Tools"
description: "Part Five of Phase 03"
lead: "What are we actually using?"
date: 2023-03-24T09:21:01-04:00
lastmod: 2023-03-24T09:21:01-04:00
draft: false 
images: []
menu:
  docs:
    parent: ""
    identifier: "tools-b96572a839e61249a0c13cd88fdc85da"
weight: 411 
toc: true
---

## Bash Scripts
As of now, Bash scripts are the life blood of FAANG | LESS. 
The entire kit is powered by Bash scripts.

With most Linux distributions, including our currently-targetted Ubuntu, using Bash as their default shell, functionality should be guaranteed. 

## Docker
Containerisation is an important thing when it comes to running software on different computers. 

While we are currently not developing server software, a lot of the software we hope to help people install and run are already packaged in Docker containers. 
Utilising official, or large-community supported, containers will help ensure services run as expected. 

## Tailscale
Opening computers to the public internet is not always the safest thing to do. 
We understand that a lot of our users will be new to the topic, so we don't want to add another thing for them to worry about. 

With this in mind, we decided it best to utilise VPN technologies to limit open connections. 
For the time being, we decided on Tailscale as a good balance for most people, though it does rely on third-parties. 

We hope to be able to, in the future, also support native Wireguard and/or OpenVPN. 
A further out goal, which may simply be too far reaching, is to have the proper documentation and instructions readily available to help users in being able to have services be public-facing. 

## NGINX
Mainly selected for its reverse proxy, NGINX will be able to help users not need to memorise long IP addresses and port numbers. 
Instead, we hope to put everything behind a single, local domain. 
