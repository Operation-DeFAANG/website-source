---
title: "Introduction"
description: "Introducing Phase 02"
lead: "Where the fun begins"
date: 2022-11-06T16:50:05-05:00
lastmod: 2022-11-06T16:50:05-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "introduction-3b9bc24c95b8dfe34a1a0130824e48e4"
weight: 310
toc: true
---

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

## Phase 02
The purpose of this phase is pretty self-explanatory: start work on developing a prototype.

We’ve figured out who we are, how we’re going to run things, and what we’re going to do; now we need to do it.

We need to research how to accomplish our goals and any laws, codes, and/or regulations that may prevent us from doing so. 
We then need to actually start building things.

By the end, we personally hope to finish at least a functioning prototype of FAANG | LESS and a 3D model of the I-100.
