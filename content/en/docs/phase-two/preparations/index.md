---
title: "Preparations"
description: "Parts Three, Four, Six, Nine, and Ten of Phase 02"
lead: "Before the prototype..."
date: 2022-12-13T13:12:24-05:00
lastmod: 2022-12-13T13:12:24-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "preparations-22d348ed161fb97b71381732d3bb2230"
weight: 313
toc: true
---

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

## Sketching
Before we could properly start developing, sketches were in order.

We spent some time on both basic and technical drawings of what we wanted of FAANG | LESS and the I-100.

### Basic
The basic sketches were very simple and preliminary; we hadn't really started work on either products yet.

#### FAANG | LESS
![FAANG-LESS Sketch](FAANG-LESS-Sketch.png "Basic sketch of FAANG | LESS's dashboard")

#### I-100
![I-100 Sketch](I-100-Sketch.png "Basic isometric sketch of the I-100")

### Technical
The technical drawings were made as we were starting work on FAANG | LESS.

#### FAANG | LESS
![FAANG-LESS Storyboard](FAANG-LESS-Storyboard.png "Storyboard of FAANG | LESS installation process")

## Decisions
Throughout the process of development, a lot of decisions had to be made. They ranged from simple, immediate things, such as what/how we presented for our Critique 02 presentation, to bigger, long-term issues, like what we'd be writing FAANG | LESS in.

We also had to make a tough decision as to if we'd continue support for the I-100. The decision was that, for the duration of the school Expo, we would be halting development.
