---
title: "Research"
description: "Part Four of Phase 01; Parts Five, Seven, and Eight of Phase 02"
lead: "More information was needed before we began"
date: 2022-12-13T12:25:37-05:00
lastmod: 2022-12-13T12:25:37-05:00
draft: false 
images: []
menu:
  docs:
    parent: ""
    identifier: "research-acd583df0242b2c19a7402dafb1d6bdd"
weight: 311
toc: true
---

## Interviews
One of the best ways to gather information is to just ask people for it. This can be done through surveys, polls, interviews, or a number of other ways. 
We went with interviews due to the depths that they can dive and the greater amount of in-detail information that can be gathered through them. 
In addition to short question interviews, we conducted four in-depth interviews with people who were either interested in self-hosting or had some experience. 

The general feedback that we received was that a product like FAANG | LESS would be very useful for people new to self-hosting, but that we might want to reconsider how much of the work is done by the program. 
Some interviewees cited the learning experience and fun that they had while setting up their servers for the first time, saying that it would be nice if more people could experience that. 

We are planning to use this feedback, in additional to past and future feedback, to better our developments for the end user and their experience to both use and learn from FAANG | LESS.

## Legal Considerations
While we are certain that our products are legal, we still wanted to better understand what we were facing in terms of the law. 
We conducted simple research into primarily US law and found not much in terms of unique legal issues, mainly just laws that impact everyone, such as copyright. 

We also took a look at different privacy laws in the US, UK, and EU. 
While the US lacks strong data privacy regulations, some states -- such as California -- do. 
As we plan to attempt to host a demo, we thought that at least knowing what laws and regulations exist would be important, and knowing of California's CCPA and CPPA, the EU's GDPR, and the UK's take on GDPR will definitely be important.

## Life Cycle and Environment Impact
As our primary product is piece of software, there was not much that we could answer in terms of life cycle and environmental impact. 

We did realise, though, that if we were to end development of FAANG | LESS, only then would the life cycle truly end, or at least be on pause if someone else planned to pick up where we left off. 
The environmental impact, on the other hand, is completely dependent on the user and what hardware they choose to install FAANG | LESS on. 
