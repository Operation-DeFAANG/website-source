---
title: "Development"
description: "Part Twelve of Phase 02"
lead: "Where the magic was supposed to happen"
date: 2022-12-22T22:12:14-05:00
lastmod: 2022-12-22T22:12:14-05:00
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "development-7b4f4f7a5d33edc194c9579e4bdec53a"
weight: 314
toc: true
---

## The Plan
The was originally for us to get a large chunk of FAANG | LESS completed during this Phase, before the new year.

This did not happen. 

A large portion of the Phase was taken up by a separate class assignment, which we sadly had to give priority. 
All development had to be put on pause for this period of time. 

Following this assignment, we returned to development, but with a less ambitious plan of creating proof-of-concepts and very early, pre-pre-Alpha prototypes. 

## Prototypes
As previously stated the prototypes developed were very early development and served more so as proof-of-concepts than anything -- we needed to ensure that we could figure out how to properly develop. 

The FAANG | LESS prototype was mostly to test our ability to build GUI applications. 
We had decided to try using the Godot game engine, something that has shown to be capable of development regular system applications. 
The prototype ended up rather unflattering, though we knew early on that the odds of us going forward with it were low -- we just thought that it'd be worth spending some time to test. 

The I-100 saw a large amount of work completed on its 3D model. 
No test fittings were completed, but as a design, it turned out well. 

### Fate of the I-100 
It was during this period that we decided to halt development on the I-100. 

We had to consider a number of factors, with the primary one being that this was first and foremost a school assignment. 
With us having discussed this early on, we made a swift decision to halt development on the I-100 for the time being. 

Given that the Expo required us to present what we made, we determined that for presentablility and overall focus, it would be best to restructure with FAANG | LESS getting our entire attention. 
