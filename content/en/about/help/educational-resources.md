---
title: "Educational Resources"
description: "Learning the Solutions to Our Problems"
lead: "Help us learn what you think we need help on!"
date: 2020-11-12T15:22:20+01:00
lastmod: 2020-11-12T15:22:20+01:00
draft: false
images: []
menu: 
  about:
    parent: "help"
weight: 620
toc: true
---

## Overview 
We are a group of students. We are new to a lot of this. 

While we know some things about what we're working on, we don't know too much. Any resources could help.

## Development

### FAANG | LESS
We're currently using [Godot](https://godotengine.org/) to build our applications. 

While we do know of great resources, such as the [official documentation](https://docs.godotengine.org/), the [Godot community](https://godotengine.org/community), and [GDQuest](https://www.gdquest.com/), they are all primarily focused on using the game engine to, well, develop games. 

If you know of any resources to learn regular application development with Godot, that would be of great help. 

We are also still open to change what we use -- it _is_ still early days after all. 

### I-100

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

<br />
For this, we are near completely in the dark, to be honest. 

We plan on using free and open source tools, like [Blender](https://www.blender.org/) and [FreeCAD](https://www.freecadweb.org/), to develop the models. The problem is, we are not extremely knowledgeable and experienced with these.
We are planning on learning using their documentation and online resources that we can find though.

If you know of any alternative to these or any specific resources, that we be greatly helpful. 

## Mentorship
As said before, we are still very much in the learning phase of this project. 

While online resources are great, a person to go to would be better. 
A mentor's role and responsibilities are relatively light and vague: help us, Operation DeFAANG, out. 

They don't need to work a specific job, live in a specific place, or be always available. We just need someone who knows what they are talking about and is willing to teach us what they know. 
Specifically, we are looking for people with knowledge in software development, systems administration, 3D modeling and printing, and manufacturing.

