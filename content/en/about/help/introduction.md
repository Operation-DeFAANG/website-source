---
title: "Introduction"
description: "How to Help Operation DeFAANG"
lead: "There are many ways to help Operation DeFAANG"
date: 2020-11-12T13:26:54+01:00
lastmod: 2020-11-12T13:26:54+01:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 610
toc: true
---

{{< alert icon="💡" text="Operation DeFAANG is currently a school project and cannot receive direct development contributions" />}}

## Educational Resources 
Operation DeFAANG is still a growing group with a lot to learn. A relatively simple way to help is to direct us to resources for things to learn. 

These things don't need to be special -- they can be anything that you think we need help with, whether that be development, marketing, or just organisation.

## Mentorship
Something that the project requires and that we could greatly benefit from is mentorship -- there's a lot for us to learn, an expert could be of great help. 

Mentorship may seem very formal, but it doesn't have to be. Little bits here and there could go a long way. 

## Sponsorship
While the project aims to provide for free and to remain open source, funding could be of great use for accessing content behind paywalls. 

Additionally, funding could help Operation DeFAANG build more prototypes and test on more machines to ensure greater support and stability.
