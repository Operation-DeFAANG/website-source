---
title: "Funding"
description: "Help Fund Operation DeFAANG"
lead: "Funding is not a necessity, rather a nice to have"
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 630
toc: true
---

{{< alert icon="⚠️" text="Funding would have entirely been for the I-100. As development of the case has been halted, no funding is currently necessary" />}}

## Sponsorships
A sponsor would be anyone who could help us out in a financial way, whether that be giving us money or giving us something so that we wouldn’t need to spend our own.

What we are currently look for is ITX computer parts, old computers, and materials for manufacturing, whether that be simple 3D printers and printing material or wood and metal.

## How It'll Be Used

### Money
Money provided to us would be used to acquire items that we require. 

At the moment, these would include a proper domain name, computer parts for testing, a 3D printer and 3D printing material for case development, and extra physical materials, like wood and aluminium, also for case development. 

### Computers and Computer Parts
Computers and computer parts would be used for testing our software and case to ensure stability and support on as wide a range of hardware as possible. 

At the moment, we are looking for an ITX or otherwise small or low-profile computer components from within the last 15 years to develop and test on.
The hardware would be test fitted into the case and used to determine if we hit certain thermal and auditory targets. 

Old computer hardware would also be useful. We hope to help reduce the number of unused computers lying in corners of homes by giving them a purpose: host services.

### Other Materials and Items
Materials and items, such as 3D printers and 3D printing material, would be used to develop physical prototypes of the I-100 case. 
Without such materials, we would be limited to 3D models, which would not be very helpful in testing. 

The goal is to make prototypes out of at least three materials to ensure stability across different potential materials end users might make the case from. 

## Reporting Usage
Transparency is very important to us. 

We plan on reporting out usage of sponsorship funds as best we can on our website as we receive and use them.

This will be shown under each sponsor in the [Sponsors]({{< ref "sponsors/" >}} "Sponsors") page.
