---
title: "Sponsors"
description: "Operation DeFAANG's Sponsors"
lead: "Brought to you with help from..."
date: 2022-11-09T17:56:17-05:00
lastmod: 2022-11-09T17:56:17-05:00
draft: false
images: []
menu:
  about:
    parent: "help"
weight: 640
toc: true
---

## Definition
> a person or an organization that pays for or plans and carries out a project or activity

“sponsor,” _Merriam-Webster.com Dictionary_, https://www.merriam-webster.com/dictionary/sponsor.

People and companies who financial support Operation DeFAANG in some way, shape, or form will be listed here. 

## Sponsors

### E2D
<img src="E2D-Logo.png" alt="E2D Logo" style="width:400px; margin-left:auto; margin-right:auto; display:block;"/>

Based in North Carolina, [E2D](https://www.e-2-d.org/), or Eliminate the Digital Divide, is a company that works in refurbishing laptops to provide to families without access to technology. 
E2D aims to eliminate the digital divide, as their name states, by ensuring all families have access to computers and internet.

E2D provided Operation DeFAANG with unused laptops to help test FAANG | LESS on a wide range of system specifications.
