---
title: "FAANG | LESS"
description: "An all-in-one tool to set up and manage a server"
lead: "Dislike the terminal? This all-in-one tool will help you set up and manage your server"
date: 2022-11-03T13:08:43-04:00
lastmod: 2022-11-03T13:08:43-04:00
draft: false 
images: []
menu:
  docs:
    parent: ""
    identifier: "faang-less-2df8ad9dcc6e0524243bf88471fbf516"
weight: 110
toc: true
---

## Goal
Self-hosting is a very tedious and, in some cases, difficult task, both to set up and especially to manage.

FAANG | LESS aims to make the whole process easier by abstracting everything behind simple graphical interfaces.

## Technology
### Front-End
FAANG | LESS is built with [Godot](https://godotengine.org/). 

### Back-End
FAANG | LESS is planned to use [Docker](https://www.docker.com/) and [Ansible](https://www.ansible.com/) for its applications and services.

## Screenshots
_[to be added]_
