---
title: "Introduction"
description: "Introducing Operation DeFAANG"
lead: "Handing back privacy and control"
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "prologue"
weight: 100
toc: true
---

## A Modern Problem
Big Tech companies don’t allow users to escape the collection and storage of personal data. Self hosting is a good alternative, however, manually setting up and configuring a server is exceedingly tedious and time consuming – especially for first timers. Lack of understanding of the command line prevents and wards away individuals from self-hosting.

## A Modern Solution
Big Tech’s services might be convenient, but have you thought about the privacy implications and control of so many people using them? Facebook and Google are clear examples with the Facebook-Cambridge Analytica 2016 election scandal and Google being largely funded by selling data. 

FAANG | LESS is trying to take a jab at that market share and control by helping users set up their own home servers to run things similar to what these tech giants offer. We are aiming to provide a quick and easy way for individuals to host their own Google Drives on their own computers. 
