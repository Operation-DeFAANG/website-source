---
title: "Introduction"
description: "How The Project Is Run"
lead: "Students learning as they go"
date: 2022-11-03T13:15:16-04:00
lastmod: 2022-11-03T13:15:16-04:00
draft: false
images: []
menu:
  docs:
    parent: "overview"
    identifier: "introduction-9333f4e0dab58bec412481c2a49be408"
weight: 200 
toc: true
---

{{< alert icon="⚠️" text="Development of the I-100 has been halted and may not be continued for the duration of the project" />}}

## Operation DeFAANG
Operation DeFAANG is a group of four students working together to build, currently, FAANG | LESS and the I-100. 

The students don't have particular special training and are learning a lot as they go, relying on documentation and support from mentors. 
