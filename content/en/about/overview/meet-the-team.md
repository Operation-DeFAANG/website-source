---
title: "Meet the Team"
description: "Meet Operation DeFAANG"
lead: "Every project has a team behind it"
date: 2022-11-06T16:55:26-05:00
lastmod: 2022-11-06T16:55:26-05:00
draft: false
images: []
menu:
  about:
    parent: ""
weight: 210
toc: true
---

## Operation DeFAANG
Operation DeFAANG is a student group. 

Let's meet the students. 

### Andy H
Hello there! I’m Andy. I started Operation DeFAANG and currently act as project manager. 

As project manager, I generally lead and direct the course of the project while helping everyone else where they need it.

I can be contacted at aha.t [at] simplelogin [dot] co.

### Calvin H
Hey! I'm Calvin. I work on art and design. 

For most things related to aesthetics, I help work on. 
Currently, I'm working on the I-100.

### Joshua G
Hello! I'm Josh. I work on research and development. 

I'm in charge of most of the development work that is done here and am working primarily on FAANG | LESS. 

### Luki L
Hey! I'm Luki. I handle most of the project's communications and outreach to third-parties. 

I reach out to individuals and organisations for support, whether that be through mentorship or sponsorship. 
